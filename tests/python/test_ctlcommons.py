# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import sys
import unittest

from unittest.mock import patch

from opentf.tools.ctlcommons import _is_command, _get_value, _get_columns, _ensure_uuid


########################################################################
# Datasets

UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'

########################################################################
# Helpers


class TestCtlCommons(unittest.TestCase):
    """Unit tests for ctlcommons."""

    # _is_command

    def test_is_command_1(self):
        result = _is_command('_', ['', 'foo'])
        self.assertTrue(result)

    def test_is_command_2(self):
        result = _is_command('foo', ['', 'foo'])
        self.assertTrue(result)

    def test_is_command_3(self):
        result = _is_command('foo bar', ['', 'foo'])
        self.assertFalse(result)

    def test_is_command_4(self):
        result = _is_command('_ _', ['', 'foo'])
        self.assertFalse(result)

    def test_is_command_5(self):
        result = _is_command('_ foo', ['', 'foo'])
        self.assertFalse(result)

    def test_is_command_6(self):
        result = _is_command('_ foo', ['', 'bar', 'foo'])
        self.assertTrue(result)

    def test_is_command_7(self):
        result = _is_command('_ bar', ['', 'bar', 'foo'])
        self.assertFalse(result)

    # _get_value

    def test_get_value_1(self):
        args = ['', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_value('--yada=')
        self.assertIsNone(result)

    def test_get_value_2(self):
        args = ['', '--yada=bar']
        with patch.object(sys, 'argv', args):
            result = _get_value('--yada=')
        self.assertEqual(result, 'bar')

    def test_get_value_3(self):
        args = ['', '--yada=bar', '--yada=baz']
        with patch.object(sys, 'argv', args):
            result = _get_value('--yada=')
        self.assertEqual(result, 'bar')

    def test_get_value_4(self):
        args = ['', '--yada_foo=1']
        with patch.object(sys, 'argv', args):
            result = _get_value('--yada-foo=')
        self.assertEqual(result, '1')

    def test_get_value_5(self):
        args = ['', '--yada-foo=1']
        with patch.object(sys, 'argv', args):
            result = _get_value('--yada-foo=')
        self.assertEqual(result, '1')

    # _get_columns

    def test_get_columns_1(self):
        args = ['']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__DEFAULT__')

    def test_get_columns_2(self):
        args = ['', '-o', 'wide']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__WIDE__')

    def test_get_columns_3(self):
        args = ['', '--output=wide', 'yada']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__WIDE__')

    def test_get_columns_4(self):
        args = ['', '-o', 'notwide']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__DEFAULT__')

    def test_get_columns_5(self):
        args = ['', 'custom-columns=yada']
        with patch.object(sys, 'argv', args):
            self.assertRaises(
                ValueError, _get_columns, wide='__WIDE__', default='__DEFAULT__'
            )

    def test_get_columns_6(self):
        args = ['', '--output=custom-columns=yada']
        with patch.object(sys, 'argv', args):
            self.assertRaises(
                ValueError, _get_columns, wide='__WIDE__', default='__DEFAULT__'
            )

    def test_get_columns_7(self):
        args = ['', '--output=custom-columns=yada:yoda']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, ['yada:yoda'])

    # _ensure_uuid

    def test_ensure_uuid_1(self):
        self.assertRaises(SystemExit, _ensure_uuid, 'not an uuid')

    def test_ensure_uuid_2(self):
        self.assertIsNone(_ensure_uuid(UUID_OK))
