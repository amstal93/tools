# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

import yaml

from opentf.tools.ctlconfig import (
    _get_port,
    config_cmd,
    print_config_help,
    read_configuration,
    use_context,
    set_context,
    delete_context,
    set_credentials,
    delete_credentials,
    set_orchestrator,
    delete_orchestrator,
    view_config,
    generate_config,
    _fatal_cannot_modify_configuration_file,
)


########################################################################
# Datasets

OPENTF_CONFIG_YAML = '''
apiVersion: opentestfactory.org/v1alpha1
contexts:
- context:
    orchestrator: local
    user: local
  name: local
current-context: local
kind: CtlConfig
orchestrators:
- name: local
  orchestrator:
    insecure-skip-tls-verify: true
    ports:
      agentchannel: 9366
      eventbus: 8765
      killswitch: 9465
      observer: 9565
      receptionist: 8865
    server: http://localhost
users:
- name: local
  user:
    token: zz
'''

CONFIG = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'ports': {
            'eventbus': 1234,
            'receptionist': 4321,
            'observer': 9999,
            'killswitch': 8888,
        },
        'server': 'aaaa',
    },
}


########################################################################
# Helpers


def _make_mock_read_opentfconfig(config=None):
    def _mro():
        return ('/mock/opentfconfig', config)

    if config is None:
        config = yaml.safe_load(OPENTF_CONFIG_YAML)

    return _mro


def _mock_write_opentfconfig(conf_filename, config):
    return


class TestCtlConfig(unittest.TestCase):
    """Unit tests for ctlconfig."""

    ## ctlconfig

    # _get_port

    def test_get_port_1(self):
        args = ['']
        with patch.object(sys, 'argv', args), patch('builtins.input', lambda *_: ''):
            result = _get_port('myservice', 12)
        self.assertEqual(result, 12)

    def test_get_port_2(self):
        args = ['', '--orchestrator-myservice-port=11']
        with patch.object(sys, 'argv', args):
            result = _get_port('myservice', 12)
        self.assertEqual(result, 11)

    def test_get_port_3(self):
        args = ['', '--orchestrator-myservice-port=noway']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, _get_port, 'myservice', 12)

    # config_cmd

    def test_config_cmd_0(self):
        args = ['', 'config']
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch('builtins.print', mock):
            config_cmd()
        mock.assert_called()

    def test_config_cmd_1(self):
        args = ['', 'config', 'unknown']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, config_cmd)

    def test_config_cmd_2(self):
        args = ['', 'config', 'view']
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.view_config', lambda: None
        ):
            config_cmd()

    def test_config_cmd_3(self):
        args = ['', 'config', 'delete-orchestrator', 'local']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            result = config_cmd()
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 0)
        self.assertEqual(len(config['users']), 1)

    def test_config_cmd_4(self):
        args = ['', 'config', 'generate']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.generate_config', mock
        ):
            result = config_cmd()
        mock.assert_called_once()

    def test_config_cmd_5(self):
        context_id = 'cOnTEXt'
        args = ['', 'config', 'use-context', context_id]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.use_context', mock
        ):
            result = config_cmd()
        mock.assert_called_once_with(context_id)

    def test_config_cmd_6(self):
        context_id = 'cOnTEXt'
        args = ['', 'config', 'set-context', context_id]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.set_context', mock
        ):
            result = config_cmd()
        mock.assert_called_once_with(context_id)

    def test_config_cmd_7(self):
        orchestrator_id = 'orCheSTRatoR'
        args = ['', 'config', 'set-orchestrator', orchestrator_id]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.set_orchestrator', mock
        ):
            result = config_cmd()
        mock.assert_called_once_with(orchestrator_id)

    def test_config_cmd_8(self):
        credentials_id = 'cREdenTIALs'
        args = ['', 'config', 'set-credentials', credentials_id]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.set_credentials', mock
        ):
            result = config_cmd()
        mock.assert_called_once_with(credentials_id)

    def test_config_cmd_9(self):
        credentials_id = 'cREdenTIALs'
        args = ['', 'config', 'delete-credentials', credentials_id]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.delete_credentials', mock
        ):
            result = config_cmd()
        mock.assert_called_once_with(credentials_id)

    def test_config_cmd_10(self):
        context_id = 'cOnTEXt'
        args = ['', 'config', 'delete-context', context_id]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        mock = MagicMock()
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlconfig.delete_context', mock
        ):
            result = config_cmd()
        mock.assert_called_once_with(context_id)

    # print_config_help

    def test_print_config_help_1(self):
        args = ['', 'unknown']
        self.assertRaises(SystemExit, print_config_help, args)

    def test_print_config_help_2(self):
        args = ['', 'config', 'generate']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_config_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_config_help_3(self):
        args = ['', 'config']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_config_help(args)
        self.assertTrue(mock.call_count > 0)

    # read_configuration

    def test_read_configuration_1(self):
        mock = MagicMock(return_value='')
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(
            sys, 'exit', mock
        ):
            read_configuration()
        self.assertEqual(mock.call_count, 0)

    def test_read_configuration_2(self):
        args = ['', '--user=yada']
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(
            sys, 'argv', args
        ):
            self.assertRaises(SystemExit, read_configuration)

    def test_read_configuration_3(self):
        args = ['', '--user=']
        mock = MagicMock(return_value='')
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(
            sys, 'exit', mock
        ), patch.object(
            sys, 'argv', args
        ):
            read_configuration()
        mock.assert_called_once()

    def test_read_configuration_4(self):
        args = ['', '--orchestrator=local']
        mock = MagicMock(return_value='')
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(
            sys, 'exit', mock
        ), patch.object(
            sys, 'argv', args
        ):
            read_configuration()
        mock.assert_not_called()

    def test_read_configuration_5(self):
        args = ['']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['contexts'][0]['context']['orchestrator']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, read_configuration)

    def test_read_configuration_6(self):
        args = ['', '--orchestrator=local']
        mock = MagicMock(return_value='')
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['contexts'][0]['context']['orchestrator']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            read_configuration()
        mock.assert_not_called()

    def test_read_configuration_7(self):
        args = ['']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['contexts'][0]['context']['user']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, read_configuration)

    def test_read_configuration_8(self):
        args = ['', '--user=local']
        mock = MagicMock(return_value='')
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['contexts'][0]['context']['user']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            read_configuration()
        mock.assert_not_called()

    def test_read_configuration_9(self):
        args = ['']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['current-context']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, read_configuration)

    def test_read_configuration_10(self):
        args = ['', '--context=local']
        mock = MagicMock(return_value='')
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['current-context']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            read_configuration()
        mock.assert_not_called()

    # use_context

    def test_use_context_nok(self):
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ):
            self.assertRaises(SystemExit, use_context, 'unknown_context')

    def test_use_context_savefailed(self):
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig',
            MagicMock(side_effect=Exception('ooOps')),
        ):
            self.assertRaises(SystemExit, use_context, 'local')

    def test_use_context_ok(self):
        mock = MagicMock(return_value='')
        with patch(
            'opentf.tools.ctlconfig._read_opentfconfig', _make_mock_read_opentfconfig()
        ), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(
            sys, 'exit', mock
        ):
            result = use_context('local')
        self.assertIsNone(result)
        mock.assert_not_called()

    # set_context

    def test_set_context_1(self):
        mock = MagicMock(return_value='')
        args = ['', '--user=local']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            result = set_context('newlocal')
        self.assertIsNone(result)
        mock.assert_not_called()
        self.assertEqual(len(config['contexts']), 2)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_set_context_2(self):
        mock = MagicMock(return_value='')
        args = ['', '--user=local', '--orchestrator=local', '--namespace=yada']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            result = set_context('newlocal')
        self.assertIsNone(result)
        mock.assert_not_called()
        self.assertEqual(len(config['contexts']), 2)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_set_context_3(self):
        mock = MagicMock(return_value='')
        args = [
            '',
            '--current',
            '--user=local',
            '--orchestrator=local',
            '--namespace=yada',
        ]
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            result = set_context('--current')
        self.assertIsNone(result)
        mock.assert_not_called()
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_set_context_4(self):
        args = ['', '--user=nono']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        del config['current-context']
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, set_context, '--current')

    def test_set_context_savefailed(self):
        args = ['', '--user=local']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig',
            MagicMock(side_effect=Exception('ooOps')),
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, set_context, 'newlocal')

    # delete_context

    def test_delete_context_1(self):
        mock = MagicMock(return_value='')
        args = ['']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'exit', mock), patch.object(sys, 'argv', args):
            result = delete_context('local')
        self.assertIsNone(result)
        mock.assert_not_called()
        self.assertEqual(len(config['contexts']), 0)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_delete_context_2(self):
        args = ['']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, delete_context, 'yada')
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_delete_savefailed(self):
        args = ['']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig',
            MagicMock(side_effect=Exception('ooOps')),
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, delete_context, 'local')

    # set_credentials

    def test_set_credentials_1(self):
        args = ['', '--token=aaaaa']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            result = set_credentials('local')
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_set_credentials_2(self):
        args = ['', '--token=aaaaa']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            result = set_credentials('newlocal')
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 2)

    def test_set_credentials_savefailed(self):
        args = ['', '--token=aaaaa']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig',
            MagicMock(side_effect=Exception('ooOps')),
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, set_credentials, 'local')

    # delete_credentials

    def test_delete_credentials_1(self):
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ):
            result = delete_credentials('local')
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 0)

    def test_delete_credentials_2(self):
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ):
            self.assertRaises(SystemExit, delete_orchestrator, 'nonexistent')
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)

    def test_delete_credentials_savefailed(self):
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig',
            MagicMock(side_effect=Exception('ooOps')),
        ):
            self.assertRaises(SystemExit, delete_credentials, 'local')

    # set_orchestrator

    def test_set_orchestrator_1(self):
        args = ['', '--server=aaaaa']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            result = set_orchestrator('local')
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)
        self.assertEqual(config['orchestrators'][0]['orchestrator']['server'], 'aaaaa')

    def test_set_orchestrator_2(self):
        args = ['', '--insecure-skip-tls-verify=false', '--observer-port=9999']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            result = set_orchestrator('nonexistent')
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 2)
        self.assertEqual(len(config['users']), 1)
        self.assertFalse(
            config['orchestrators'][1]['orchestrator']['insecure-skip-tls-verify']
        )
        self.assertEqual(
            config['orchestrators'][1]['orchestrator']['ports']['observer'], 9999
        )

    def test_set_orchestrator_3(self):
        args = ['', '--observer-port=noway']
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, set_orchestrator, 'nonexistent')

    # delete_orchestrator

    def test_delete_orchestrator_1(self):
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ):
            result = delete_orchestrator('local')
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 0)
        self.assertEqual(len(config['users']), 1)

    # view_config

    def test_view_config(self):
        config = yaml.safe_load(OPENTF_CONFIG_YAML)
        _mro = _make_mock_read_opentfconfig(config)
        with patch('opentf.tools.ctlconfig._read_opentfconfig', _mro), patch(
            'opentf.tools.ctlconfig._write_opentfconfig', _mock_write_opentfconfig
        ), patch('builtins.print', lambda *args: None):
            result = view_config()
        self.assertIsNone(result)
        self.assertEqual(len(config['contexts']), 1)
        self.assertEqual(len(config['orchestrators']), 1)
        self.assertEqual(len(config['users']), 1)
        self.assertEqual(config['users'][0]['user']['token'], 'REDACTED')

    # generate_config

    def test_generate_config(self):
        args = [
            '',
            '--name=foo',
            '--orchestrator-server=aaa',
            '--orchestrator-receptionist-port=1',
            '--orchestrator-observer-port=2',
            '--orchestrator-eventbus-port=3',
            '--orchestrator-killswitch-port=4',
            '--orchestrator-agentchannel-port=5',
            '--orchestrator-qualitygate-port=6',
            '--insecure-skip-tls-verify=true',
            '--token=tttttt',
        ]
        with patch.object(sys, 'argv', args), patch(
            'builtins.print', lambda *args: None
        ):
            generate_config()

    # _fatal_cannot_modify_configuration_file

    def test_fatal_cannot_modify_configuration_file(self):
        self.assertRaises(
            SystemExit, _fatal_cannot_modify_configuration_file, 'fiLeName', 'errOr'
        )
